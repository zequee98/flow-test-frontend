import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import config from './config';

import Layout from './layout';
import store from './layout/redux/store';

const App = function () {
  const [isLoadingApp, setIsLoadingApp] = useState(true);

  useEffect(() => {
    // Configuro la url base de axios antes de cargar la web
    axios.defaults.baseURL = `${config.API_URL}/${config.VERSION}`;
    setIsLoadingApp(false);
  }, []);

  if (isLoadingApp) return null;

  return (
    <Provider store={store}>
      <BrowserRouter>
        <Layout />
      </BrowserRouter>
    </Provider>
  );
};

export default App;
