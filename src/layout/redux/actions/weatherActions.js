import axios from 'axios';

export const FETCH_USER_CITY_WEATHER = 'FETCH_USER_CITY_WEATHER';
export const FETCH_CITY_WEATHER_BY_SEARCH = 'FETCH_CITY_WEATHER_BY_SEARCH';
export const FETCH_CITY_WEATHER_FORESCAT = 'FETCH_CITY_WEATHER_FORESCAT';

export const fetchCityWeather = () => ({
  type: FETCH_USER_CITY_WEATHER,
  payload: axios.get('/location')
    .then((response) => ({ weather: response.data }))
    .catch((error) => Promise.reject(error.response))
});

export const fetchCityWeatherBySearch = (city = '') => ({
  type: FETCH_CITY_WEATHER_BY_SEARCH,
  payload: axios.get(`/current/${city}`)
    .then((response) => ({ weather: response.data }))
    .catch((error) => Promise.reject(error.response))
});

export const fetchCityWeatherForecast = (city = '') => ({
  type: FETCH_CITY_WEATHER_FORESCAT,
  payload: axios.get(`/forecast/${city}`)
    .then((response) => ({ weather: response.data }))
    .catch((error) => Promise.reject(error.response))
});
