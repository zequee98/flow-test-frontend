import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import promise from 'redux-promise-middleware';

import rootReducer from './reducers';

const middlewares = [thunk, promise];

if (process.env.NODE_ENV !== 'production') middlewares.push(createLogger());

export default createStore(
  rootReducer,
  applyMiddleware(...middlewares)
);
