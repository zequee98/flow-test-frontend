import { FETCH_CITY_WEATHER_FORESCAT } from '../actions/weatherActions';

const INITIAL_STATE = {
  isFetching: false,
  didInvalidate: false,
  errorCode: false,
  cityWeatherForescat: {
    oneWeatherPerDay: []
  },
};

const cityWeatherForescatReducer = (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {

  case `${FETCH_CITY_WEATHER_FORESCAT}_REJECTED`:
    return {
      ...state,
      isFetching: false,
      didInvalidate: true,
      cityWeatherForescat: {
        oneWeatherPerDay: []
      },
      errorCode: payload ? payload.status : 500
    };
  case `${FETCH_CITY_WEATHER_FORESCAT}_PENDING`:
    return {
      ...state,
      isFetching: true,
      didInvalidate: false,
      errorCode: false,
    };
  case `${FETCH_CITY_WEATHER_FORESCAT}_FULFILLED`:
    return {
      ...state,
      isFetching: false,
      didInvalidate: false,
      errorCode: false,
      cityWeatherForescat: payload.weather,
    };

  default:
    return state;
  }
};

export default cityWeatherForescatReducer;
