import { FETCH_USER_CITY_WEATHER } from '../actions/weatherActions';

const INITIAL_STATE = {
  isFetching: false,
  didInvalidate: false,
  userCityWeather: false,
  errorCode: false
};

const userCityWeatherReducer = (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {

  case `${FETCH_USER_CITY_WEATHER}_REJECTED`:
    return {
      ...state,
      isFetching: false,
      didInvalidate: true,
      userCityWeather: false,
      errorCode: payload ? payload.status : 500
    };
  case `${FETCH_USER_CITY_WEATHER}_PENDING`:
    return {
      ...state,
      isFetching: true,
      didInvalidate: false,
      errorCode: false,
    };
  case `${FETCH_USER_CITY_WEATHER}_FULFILLED`:
    return {
      ...state,
      isFetching: false,
      didInvalidate: false,
      userCityWeather: payload.weather,
      errorCode: false,
    };

  default:
    return state;
  }
};

export default userCityWeatherReducer;
