import { combineReducers } from 'redux';

import cityWeatherBySearchReducer from './cityWeatherBySearchReducer';
import userCityWeatherReducer from './userCityWeatherReducer';
import cityWeatherForescatReducer from './cityWeatherForescatReducer';

const rootReducer = combineReducers({
  cityWeatherBySearchReducer,
  userCityWeatherReducer,
  cityWeatherForescatReducer
});

export default rootReducer;
