import { FETCH_CITY_WEATHER_BY_SEARCH } from '../actions/weatherActions';

const INITIAL_STATE = {
  isFetching: false,
  didInvalidate: false,
  citySearchedWeather: false,
  errorCode: false
};

const cityWeatherBySearchReducer = (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {

  case `${FETCH_CITY_WEATHER_BY_SEARCH}_REJECTED`:
    return {
      ...state,
      isFetching: false,
      didInvalidate: true,
      citySearchedWeather: false,
      errorCode: payload ? payload.status : 500
    };
  case `${FETCH_CITY_WEATHER_BY_SEARCH}_PENDING`:
    return {
      ...state,
      isFetching: true,
      didInvalidate: false,
      errorCode: false,
    };
  case `${FETCH_CITY_WEATHER_BY_SEARCH}_FULFILLED`:
    return {
      ...state,
      isFetching: false,
      didInvalidate: false,
      errorCode: false,
      citySearchedWeather: payload.weather
    };

  default:
    return state;
  }
};

export default cityWeatherBySearchReducer;
