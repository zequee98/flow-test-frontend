/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import clsx from 'clsx';
import { Switch, Route, Redirect } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';

import config from '../../config';

import UserCityWeather from '../ui/userCityWeather';
import Search from '../ui/search';
import Forescat from '../ui/forescat';

const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -config.DRAWER_WIDTH,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  toolbar: theme.mixins.toolbar,
}));

const RoutesContainer = function ({
  open
}) {
  const classes = useStyles();

  return (
    <main className={clsx(classes.content, { [classes.contentShift]: open })}>
      <div className={classes.toolbar} />

      <Switch>
        <Route path="/userCityWeather" component={UserCityWeather} />
        <Route path="/search" component={Search} />
        <Route path="/forecast" component={Forescat} />

        <Route exact path="*">
          <Redirect to="/userCityWeather" />
        </Route>
      </Switch>
    </main>
  );
};

export default RoutesContainer;
