import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';

import Drawer from './commons/drawer';
import AppBar from './commons/appBar';
import Routes from './routes';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flex: 1
  },
  toolbar: theme.mixins.toolbar,
}));

const LayoutContainer = function () {
  const classes = useStyles();
  const [open, setOpen] = useState(true);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        open={open}
        handleDrawerOpen={handleDrawerOpen}
      />
      <Drawer
        open={open}
        handleDrawerClose={handleDrawerClose}
      />
      <Routes
        open={open}
      />
    </div>
  );
};

export default LayoutContainer;
