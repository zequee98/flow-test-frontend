import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Card, CardContent } from '@material-ui/core';

import { fetchCityWeatherBySearch } from '../../redux/actions/weatherActions';
import LoadingComponent from '../../commons/loading/loadingComponent';
import WeatherComponent from '../../commons/weather';
import SearchComponent from '../../commons/searchForm';
import ErrorHandling from '../../commons/errorHandling';

const LocationContainer = function () {
  const dispatch = useDispatch();
  const cityData = useSelector((state) => state.cityWeatherBySearchReducer);
  const {
    isFetching,
    citySearchedWeather,
    didInvalidate,
    errorCode
  } = cityData;

  const handleSubmit = (text) => {
    dispatch(fetchCityWeatherBySearch(text));
  };

  if (didInvalidate && errorCode === 500) return <ErrorHandling />;

  return (
    <Card>
      <CardContent>
        <SearchComponent
          onSubmit={handleSubmit}
          isFetching={isFetching}
        />

        {isFetching && <LoadingComponent />}

        {(citySearchedWeather.weather || errorCode) && (
          <WeatherComponent
            data={citySearchedWeather}
            errorCode={errorCode}
          />
        )}
      </CardContent>
    </Card>
  );
};

export default LocationContainer;
