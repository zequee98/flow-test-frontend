import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Card, CardContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { fetchCityWeatherForecast } from '../../redux/actions/weatherActions';
import LoadingComponent from '../../commons/loading/loadingComponent';
import WeatherComponent from '../../commons/weather';
import SearchComponent from '../../commons/searchForm';
import ErrorHandling from '../../commons/errorHandling';

const useStyles = makeStyles((theme) => ({
  card: {
    marginBottom: theme.spacing(4),
  },
}));

const LocationContainer = function () {
  const classes = useStyles();
  const dispatch = useDispatch();
  const cityData = useSelector((state) => state.cityWeatherForescatReducer);
  const {
    isFetching,
    cityWeatherForescat,
    didInvalidate,
    errorCode
  } = cityData;

  const handleSubmit = (text) => {
    dispatch(fetchCityWeatherForecast(text));
  };

  if (didInvalidate && errorCode === 500) return <ErrorHandling />;

  return (
    <Card className={classes.card}>
      <CardContent>
        <SearchComponent
          onSubmit={handleSubmit}
          isFetching={isFetching}
        />

        {isFetching && <LoadingComponent />}

        {cityWeatherForescat.cityName && (<h1>{cityWeatherForescat.cityName}</h1>)}

        {cityWeatherForescat.oneWeatherPerDay.map((weatherPerDay) => (
          <WeatherComponent
            forescat
            key={weatherPerDay.dt_txt}
            errorCode={errorCode}
            data={{
              temp: weatherPerDay.main,
              weather: weatherPerDay.weather[0]
            }}
          />
        ))}

        {errorCode && (<ErrorHandling errorCode={errorCode} />)}

      </CardContent>
    </Card>
  );
};

export default LocationContainer;
