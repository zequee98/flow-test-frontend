import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Card, CardContent, Button } from '@material-ui/core';

import { fetchCityWeather } from '../../redux/actions/weatherActions';
import LoadingComponent from '../../commons/loading/loadingComponent';
import ErrorHandling from '../../commons/errorHandling';
import WeatherComponent from '../../commons/weather';

const UserCityWeatherContainer = function () {
  const dispatch = useDispatch();
  const cityData = useSelector((state) => state.userCityWeatherReducer);
  const {
    isFetching,
    userCityWeather,
    didInvalidate,
    errorCode
  } = cityData;

  useEffect(() => {
    // Verifico que no haya data cargada para evitar pedidos innecesarios.
    if (!userCityWeather.weather && !isFetching && !didInvalidate) {
      dispatch(fetchCityWeather());
    }
  }, [cityData, dispatch, didInvalidate, userCityWeather, isFetching]);

  const refreshUserWeather = () => {
    dispatch(fetchCityWeather());
  };

  if (didInvalidate && errorCode === 500) return <ErrorHandling />;

  return (
    <Card>
      <CardContent>
        {userCityWeather.weather && (
          <Button
            variant="contained"
            color="primary"
            style={{ margin: 16 }}
            onClick={refreshUserWeather}
            disabled={isFetching}
          >
            Refrescar clima
          </Button>
        )}

        {isFetching
          ? (<LoadingComponent />)
          : (<WeatherComponent data={userCityWeather} errorCode={errorCode} />)}

      </CardContent>
    </Card>
  );
};

export default UserCityWeatherContainer;
