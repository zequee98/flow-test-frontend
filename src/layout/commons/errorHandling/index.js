import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import { Error } from '@material-ui/icons';

const useStyles = makeStyles(() => ({
  container: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    height: 400,
    alignItems: 'center'
  },
  iconContainer: {
    display: 'flex',
    width: 200
  },
  icon: {
    fontSize: 150,
    color: '#E5006A'
  },
  content: {
    maxWidth: 500,
    marginLeft: 16
  },
  title: {
    color: '#E5006A',
    fontFamily: "'Sarabun', sans-serif",
    fontStyle: 'normal',
    fontWeight: '600',
    letterSpacing: 0.5
  },
  text: {
    color: '#171D33',
    fontFamily: "'Sarabun', sans-serif",
    fontStyle: 'normal',
    fontWeight: '500',
    letterSpacing: 0.2
  },
  buttonContainer: {
    marginTop: 32,
  },
}));

const ErrorHandling = function ({
  errorCode
}) {
  const classes = useStyles();

  if (errorCode === 404) return (<h1>No se encontraron datos</h1>);
  if (errorCode === 400) return (<h1>El campo ingresado es invalido</h1>);

  return (
    <div className={classes.container}>
      <div className={classes.iconContainer}>
        <Error className={classes.icon} />
      </div>
      <div className={classes.content}>

        <h1 className={classes.title}>
          Upss...
        </h1>

        <p className={classes.text}>
          Lo sentimos, algo salió mal, por favor, intente nuevamente.
        </p>

        <div className={classes.buttonContainer}>
          <Button variant="contained" color="primary" disableElevation onClick={() => window.location.reload()}>
            Refrescar
          </Button>
        </div>

      </div>
    </div>
  );
};

export default ErrorHandling;
