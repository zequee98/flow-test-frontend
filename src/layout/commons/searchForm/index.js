import React, { useState } from 'react';
import { TextField, Button } from '@material-ui/core';

const SearchFormComponent = function ({
  onSubmit,
  isFetching
}) {
  const [keyword, setKeyword] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();

    onSubmit(keyword);
  };

  const handleChange = (event) => {
    setKeyword(event.target.value);
  };

  return (
    <form style={{ display: 'flex', alignItems: 'center' }} onSubmit={handleSubmit}>
      <TextField
        variant="outlined"
        placeholder="Buscar clima por nombre de la ciudad"
        label="Buscar"
        fullWidth
        onChange={handleChange}
        value={keyword}
      />
      <Button
        variant="contained"
        color="primary"
        type="submit"
        style={{ margin: 16 }}
        disabled={isFetching}
      >
        Buscar
      </Button>
    </form>
  );
};

export default SearchFormComponent;
