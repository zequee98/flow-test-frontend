import React from 'react';
import { Link } from 'react-router-dom';
import {
  ListItem,
  ListItemIcon,
  ListItemText
} from '@material-ui/core';

export default function ListItemComponent({
  path,
  text,
  icon: Icon,
}) {
  return (
    <ListItem
      button
      component={Link}
      to={path}
    >
      <ListItemIcon>
        <Icon />
      </ListItemIcon>
      <ListItemText primary={text} />
    </ListItem>
  );
}
