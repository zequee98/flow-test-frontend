import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {
  ChevronLeft,
  ChevronRight,
  MoveToInbox,
  Search
} from '@material-ui/icons';
import {
  Drawer,
  List,
  Divider,
  IconButton,
} from '@material-ui/core';

import config from '../../../config';

import ListItemComponent from './utils/listItemDrawer';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    width: config.DRAWER_WIDTH,
    flexShrink: 0,
  },
  drawerPaper: {
    width: config.DRAWER_WIDTH,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
}));

export default function PersistentDrawerComponent({
  open,
  handleDrawerClose
}) {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Drawer
      className={classes.drawer}
      variant="persistent"
      anchor="left"
      open={open}
      classes={{ paper: classes.drawerPaper }}
    >
      <div className={classes.drawerHeader}>
        <IconButton onClick={handleDrawerClose}>
          {theme.direction === 'ltr' ? <ChevronLeft /> : <ChevronRight />}
        </IconButton>
      </div>

      <Divider />

      <List>
        <ListItemComponent
          path="/"
          text="Api: /location"
          icon={MoveToInbox}
        />
        <ListItemComponent
          path="/search"
          text="Api: /current"
          icon={Search}
        />
        <ListItemComponent
          path="/forecast"
          text="Api: /forecast"
          icon={Search}
        />
      </List>
    </Drawer>
  );
}
