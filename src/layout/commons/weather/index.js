import React from 'react';
import ErrorHandling from '../errorHandling';

const WeatherComponent = function ({
  data,
  forescat,
  errorCode
}) {
  if (errorCode) return (<ErrorHandling errorCode={errorCode} />);

  const { cityName, temp, weather } = data;
  const celciusTemp = (tempToConvert) => (tempToConvert - 273.15).toFixed(2);

  return (
    <div style={{ marginTop: 32 }}>
      {!forescat && (
        <h1>
          Ciudad: {cityName || '---'}
        </h1>
      )}
      {!forescat && (
        <h1>
          Temperatura actual: {temp ? celciusTemp(temp.temp) : 0} grados
        </h1>
      )}
      <h1>
        Temperatura máxima: {temp ? celciusTemp(temp.temp_max) : 0} grados
      </h1>
      <h1>
        Temperatura mínima: {temp ? celciusTemp(temp.temp_min) : 0} grados
      </h1>
      <h1>
        Clima: {weather?.main || '---'}
      </h1>
    </div>
  );
};

export default WeatherComponent;
